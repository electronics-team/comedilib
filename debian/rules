#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1
include /usr/share/dpkg/pkg-info.mk

PYTHONS3 := $(shell py3versions -vr)
#ruby_ver = 1.8
arch_name = $(subst linux-gnu,linux-,$(patsubst %linux-gnu,%linux,$(HOST)))
#ruby_libdir  = $(CURDIR)/debian/tmp/usr/lib/ruby/$(ruby_ver)
#ruby_archdir = $(CURDIR)/debian/tmp/usr/lib/ruby/$(ruby_ver)/$(arch_name)

CPPFLAGS:=$(shell dpkg-buildflags --get CPPFLAGS)
CFLAGS:=$(shell dpkg-buildflags --get CFLAGS)
CXXFLAGS:=$(shell dpkg-buildflags --get CXXFLAGS)
LDFLAGS:=$(shell dpkg-buildflags --get LDFLAGS)

DEB_HOST_GNU_TYPE       ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE      ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)
DEB_HOST_MULTIARCH      ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
endif
ifeq (,$(findstring nostrip,$(DEB_BUILD_OPTIONS)))
	INSTALL_PROGRAM += -s
endif

export deb_udevdir = $(shell pkg-config --variable=udevdir udev | sed s,^/,,)
export DEB_BUILD_MAINT_OPTIONS = optimize=+lto

configure: configure-stamp
configure-stamp:
	dh_testdir
	dh_autoreconf
	for pyvers in ${PYTHONS3}; \
	do\
		mkdir -p pybuild/$$pyvers; \
		cp -Rl `ls . |grep -v pybuild|grep -v debian` pybuild/$$pyvers; \
		(cd pybuild/$$pyvers; \
		PYTHON="/usr/bin/python$$pyvers" ./configure \
			--host=$(DEB_HOST_GNU_TYPE) \
			--build=$(DEB_BUILD_GNU_TYPE) \
			--prefix=/usr \
			CFLAGS="$(CFLAGS)" \
			CPPFLAGS="$(CPPFLAGS)" \
			LDFLAGS="$(LDFLAGS)");\
	done
	./configure \
		--host=$(DEB_HOST_GNU_TYPE) \
		--build=$(DEB_BUILD_GNU_TYPE) \
		--prefix=/usr \
		--mandir=\$${prefix}/share/man \
		--datadir=\$${prefix}/share/doc/ \
		--sysconfdir=/etc \
		--disable-firmware \
		--localstatedir=/var \
		--disable-dependency-tracking \
		--disable-python-binding \
		--disable-ruby-binding \
		--with-pdf-backend=no\
		--with-gnu-ld \
		CFLAGS="$(CFLAGS) -fPIC" \
		CPPFLAGS="$(CPPFLAGS)" \
		LDFLAGS="$(LDFLAGS)"
	cd comedi-calibrate && CPPFLAGS="$(CPPFLAGS) -I $(CURDIR)/c++/include/ -I $(CURDIR)/include/" \
		LDFLAGS="$(LDFLAGS) -L $(CURDIR)/lib/.libs" \
		./configure --disable-comedilib-checking
	touch $@

build: build-arch build-indep
build-arch: build-arch-stamp
build-arch-stamp: configure-stamp
	dh_testdir
	$(MAKE)
	cd comedi-calibrate && $(MAKE)
	for pyvers in ${PYTHONS3};\
	do\
		rm -rf pybuild/$$pyvers/lib;\
		(cd pybuild/$$pyvers;\
		ln -s ../../lib .);\
		(cd pybuild/$$pyvers/swig/python;\
		$(MAKE);\
		sed -i '1s/^/#!\/usr\/bin\/python3\n/' comedi.py);\
	done
	touch $@

build-indep: build-indep-stamp
build-indep-stamp: configure-stamp
	touch $@

clean:
	dh_testdir
	dh_testroot
	rm -f config.log
	rm -f *-stamp
	rm -rf pybuild
	rm -rf doc/doc_html/
	rm -rf doc/man/
	rm -f lib/calib_lex.c
	rm -f lib/calib_lex.h
	rm -f lib/calib_yacc.c
	rm -f lib/calib_yacc.h
	#rm -f doc/*.xml
	rm -f doc/calibration_funcref.xml
	rm -f doc/command_funcref.xml
	rm -f doc/deprecated_funcref.xml
	rm -f rm -f doc/dio_funcref.xml
	rm -f doc/drivers.xml
	rm -f doc/error_funcref.xml
	rm -f doc/extensions_funcref.xml
	rm -f doc/funcref.xml
	[ ! -f Makefile ] || $(MAKE) distclean
	rm -f comedi-calibrate/config.log
	rm -f doc/pdf/comedilib.pdf
	find . -name Makefile -exec rm -f {} \;
	find . -name \*.o -exec rm -f {} \;
	rm -f comedi-calibrate/comedi_calibrate/comedi_calibrate
	rm -f comedi-calibrate/comedi_soft_calibrate/comedi_soft_calibrate
	rm -f comedi-calibrate/config.status
	rm -f comedi-calibrate/libcomedi_calibrate/libcomedi_calibrate.a
	[ ! -f comedi-calibrate/Makefile ]] || cd comedi-calibrate && [ ! -f Makefile ] || $(MAKE) distclean
	find . -name Makefile -exec rm -f {} \;
	dh_autoreconf_clean
	rm -rf comedi-calibrate/comedi_soft_calibrate/.deps
	rm -rf comedi-calibrate/libcomedi_calibrate/.deps
	rm -rf comedi-calibrate/comedi_calibrate/.deps
	#rm -f comedi-calibrate
	rm -f aclocal.m4
	rm -f ltmain.sh
	rm -f m4/libtool.m4
	rm -f m4/ltsugar.m4
	rm -f m4/lt~obsolete.m4
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_prep
	dh_installdirs
	$(MAKE) install prefix=$(CURDIR)/debian/tmp/usr \
		pkgdatadir=$(CURDIR)/debian/tmp/usr/share/doc/libcomedi0 \
		sysconfdir=$(CURDIR)/debian/tmp/etc \
		localstatedir=$(CURDIR)/debian/tmp/var
	cd comedi-calibrate && $(MAKE) install prefix=$(CURDIR)/debian/tmp/usr

	mkdir -p $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/comedi
	for pyvers in ${PYTHONS3};\
	do\
		(cd pybuild/$$pyvers/swig/python;\
		$(MAKE) DESTDIR=$(CURDIR)/debian/tmp install);\
		ABITAG=`python$$pyvers -c "import sysconfig; print(sysconfig.get_config_var('SOABI'))"`; \
		(cd $(CURDIR)/debian/tmp/usr/lib/; \
		 mv python$$pyvers/site-packages/comedi.py python3/dist-packages/comedi;\
		 if echo x$$ABITAG | grep -q "$(DEB_HOST_MULTIARCH)"; then \
		   mv python$$pyvers/site-packages/_comedi.so \
		      python3/dist-packages/comedi/_comedi.$$ABITAG.so;\
		 else \
		   mv python$$pyvers/site-packages/_comedi.so \
		      python3/dist-packages/comedi/_comedi.$$ABITAG-$(DEB_HOST_MULTIARCH).so;\
		 fi; \
		rm -rf python$$pyvers);\
	done
#		for f in `find debian/tmp/ -name '*.so' ! -name '*.cpython*.so'`; \
#		do mv $$f $${f%.so}.$$ABITAG.so;
#	$(foreach v,$(PYTHONS3),dh_auto_install --builddirectory=pybuild/$v \
#					   --destdir=debian/tmp/ $(nl) \
#					   ABITAG=`python$(v) -c "import sysconfig; \
#					   print(sysconfig.get_config_var('SOABI'))"`; \
#					   for f in `find debian/tmp/ -name '*.so' ! -name '*.cpython*.so'`; \
#					   do mv $$f $${f%.so}.$$ABITAG.so; done;)
	mkdir -p $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/
	mv $(CURDIR)/debian/tmp/usr/lib/pkgconfig $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/
	mkdir -p debian/tmp/$(deb_udevdir)/rules.d
	cp debian/90-comedi.rules debian/tmp/$(deb_udevdir)/rules.d/
	chmod 644 debian/tmp/usr/lib/python*/dist-packages/comedi/comedi.py
	mkdir -p debian/tmp/usr/share/doc/libcomedi-dev/demo
	cp -a demo debian/libcomedi-dev/usr/share/doc/libcomedi-dev
	rm -f debian/libcomedi-dev/usr/share/doc/libcomedi-dev/demo/Makefile*
	cp debian/Makefile.debian_demo debian/libcomedi-dev/usr/share/doc/libcomedi-dev/demo/Makefile
	cd debian/libcomedi-dev/usr/share/doc/libcomedi-dev/demo && make clean
	rm -rf debian/tmp/usr/local
	find debian/ -name _comedi.*a -exec rm -f {} \;
	cp $(CURDIR)/debian/manpages/*.8 $(CURDIR)/debian/tmp/usr/share/man/man8

binary-indep: build install

binary-arch: build install
	dh_testdir
	dh_testroot
	#
	# build libcomedi${major} package by moving files from comedilib-dev
	#
	dh_install --sourcedir=debian/tmp
	dh_missing --list-missing

	dh_installdocs
	dh_installexamples
	dh_installmenu
	dh_installcron
	dh_installinfo
	dh_installchangelogs ChangeLog
	dh_link
	dh_strip --no-automatic-dbgsym
	dh_compress --exclude=.c
	dh_python3
	for pyvers in ${PYTHONS3};\
	do\
		dh_python3 debian/python-comedilib/usr/lib/python$$pyvers;\
	done
	dh_fixperms
	dh_makeshlibs
	dh_installdeb
	dh_shlibdeps -l $(CURDIR)/debian/tmp/usr/lib/
	dh_gencontrol
	dh_md5sums
	dh_builddeb

get-orig-source:
	set -e;\
	VERCAL=5;\
	VER=0.11.0;\
	wget -q http://comedi.org/download/comedilib-$$VER.tar.gz -O comedilib_$$VER\+$$VERCAL.orig.tar.gz;\
	wget -q http://comedi.org/download/comedi_calibrate-$$VERCAL.tar.gz\
	  -O comedilib_$$VER\+$$VERCAL.orig-comedi-calibrate.tar.gz

binary: binary-indep binary-arch
.PHONY: build clean binary-indep binary-arch binary install
